package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import Database.Mariadb;

public class Order {
	private int itemId;
	private int siteId;
	private int quantity;
	private int status;
	private int orderId;
	private String itemName;
	private String siteName;
	private Timestamp createdAt;
	private String note;
	
	public Order(int itemId, int siteId, int quantity, int status, int orderId, String itemName, String siteName, Timestamp createdAt, String note) {
		this.itemId = itemId;
		this.quantity = quantity;
		this.status = status;
		this.siteId = siteId;
		this.orderId = orderId;
		this.itemName = itemName;
		this.siteName = siteName;
		this.createdAt = createdAt;
		this.note = note;
	}
	
	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getItemName() {
		return itemName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	
	public ArrayList getAvailableSite(int itemId, int numberRequired) {
		ArrayList<Order> listOrder = new ArrayList();
		String sql = "SELECT * FROM siteItem WHERE itemId = ? ORDER BY quantity DESC";
		ArrayList params = new ArrayList();
		params.add(itemId);
		ResultSet res = Mariadb.getInstance().query(sql, params);
		boolean enough = false;
		try {
			while(res.next()) {
				int siteId = res.getInt("siteId");
				int quantity = res.getInt("quantity");
				int orderNumber =  Math.min(numberRequired, quantity);
				Order order = new Order(itemId, siteId, orderNumber, 0, 0, null, null, null, "");
				listOrder.add(order);
				if(quantity >= numberRequired) {
					enough = true;
					break;
				}
				else numberRequired = numberRequired - quantity;
			};
			if(!enough) {
				return new ArrayList();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listOrder;
	}
	
	public void createOrder(int itemId, int siteId,int quantity) {
		String sql = "INSERT INTO orderItem(itemId, siteId, quantity) VALUES (?, ?, ?)";
		ArrayList params = new ArrayList();
		params.add(itemId);
		params.add(siteId);
		params.add(quantity);
		Mariadb.getInstance().query(sql, params);
	}
	public ArrayList getAllOrder() {
		ArrayList<Order> listOrder= new ArrayList();
		String sql = "SELECT OI.*, items.name as itemName, sites.name as siteName FROM orderItem OI, items, sites WHERE"
				+" items.id = OI.itemId AND sites.id = OI.siteId";
		ArrayList params = new ArrayList();
		ResultSet res = Mariadb.getInstance().query(sql, params);
		try {
			while(res.next()) {
				int id = res.getInt("id");
				int siteId = res.getInt("siteId");
				int itemId = res.getInt("itemId");
				String itemName = res.getString("itemName");
				String siteName = res.getString("siteName");
				int quantity = res.getInt("quantity");
				int status = res.getInt("status");
				String note = res.getString("note");
				Timestamp time = res.getTimestamp("createdAt");
				Order order = new Order(itemId, siteId, quantity, status, id, itemName, siteName, time, note);
				listOrder.add(order);
			};
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listOrder;
	}
	
	public ArrayList getOrderByStatus(int status) {
		ArrayList<Order> listOrder= new ArrayList();
		String sql = "SELECT OI.*, items.name as itemName, sites.name as siteName FROM orderItem OI, items, sites WHERE"
				+" items.id = OI.itemId AND sites.id = OI.siteId AND OI.status = ?";
		ArrayList params = new ArrayList();
		params.add(status);
		ResultSet res = Mariadb.getInstance().query(sql, params);
		try {
			while(res.next()) {
				int id = res.getInt("id");
				int siteId = res.getInt("siteId");
				int itemId = res.getInt("itemId");
				String itemName = res.getString("itemName");
				String siteName = res.getString("siteName");
				int quantity = res.getInt("quantity");
				Timestamp time = res.getTimestamp("createdAt");
				Order order = new Order(itemId, siteId, quantity, status, id, itemName, siteName, time, null);
				listOrder.add(order);
			};
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listOrder;
	}
	
	
	public ArrayList getOrderBySite(int siteId) {
		ArrayList<Order> listOrder= new ArrayList();
		String sql = "SELECT OI.*, items.name as itemName, sites.name as siteName FROM orderItem OI, items, sites WHERE"
				+" items.id = OI.itemId AND sites.id = OI.siteId AND sites.id = ?";
		ArrayList params = new ArrayList();
		params.add(siteId);
		ResultSet res = Mariadb.getInstance().query(sql, params);
		try {
			while(res.next()) {
				int id = res.getInt("id");
				int itemId = res.getInt("itemId");
				String itemName = res.getString("itemName");
				String siteName = res.getString("siteName");
				int status = res.getInt("status");
				int quantity = res.getInt("quantity");
				Timestamp time = res.getTimestamp("createdAt");
				Order order = new Order(itemId, siteId, quantity, status, id, itemName, siteName, time, null);
				listOrder.add(order);
			};
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listOrder;
	}
	public void updateStatusOrder(int orderId, int status) {
		String sql = "UPDATE orderItem SET status = ? WHERE id =?";
		ArrayList params = new ArrayList();
		params.add(status);
		params.add(orderId);
		Mariadb.getInstance().query(sql, params);
	}
	public void updateConfirmOrder(int orderId, int status, String note) {
		String sql = "UPDATE orderItem SET status = ?, note = ? WHERE id =?";
		ArrayList params = new ArrayList();
		params.add(status);
		params.add(note);
		params.add(orderId);
		Mariadb.getInstance().query(sql, params);
	}
}
