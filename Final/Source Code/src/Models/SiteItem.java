package Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import Database.Mariadb;
import Sites.SiteItemElementGUI;

public class SiteItem {
	private int itemId;
	private String itemName;
	private int quantity;
	private String unit;
	
	public SiteItem(int itemId, String itemName, int quantity, String unit) {
		this.itemId = itemId;
		this.itemName = itemName;
		this.quantity = quantity;
		this.unit = unit;
	}
	
	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public ArrayList getAllItem(int siteId) {

		ArrayList<SiteItem> itemArr = new ArrayList();
		String sql = "SELECT siteItem.*, items.name as name FROM siteItem, items WHERE siteItem.itemId = items.id AND siteItem.siteId = ?";
		ArrayList arr = new ArrayList();
		arr.add(siteId);
		
		ResultSet res = Mariadb.getInstance().query(sql, arr);
		try {
			while(res.next()) {
				// itemId
				int itemId = res.getInt("itemId");
				// itemName
				String itemName = res.getString("name");
				// quantity
				int quantity = res.getInt("quantity");
				// unit
				String unit = res.getString("unit");
				
				SiteItem t = new SiteItem(itemId, itemName, quantity, unit);
				itemArr.add(t);
			}	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return itemArr;
	}
	
	public void addSiteItem(ArrayList itemArr) {

		String sql = "INSERT INTO siteItem(itemId, siteId, quantity, unit) VALUES (?,?,?,?)";
		try {
			PreparedStatement preparedStatement = Mariadb.getInstance().getConn().prepareStatement(sql);
			for (int counter = 0; counter < itemArr.size(); counter++) { 	
				SiteItemElementGUI siteItem = (SiteItemElementGUI)  itemArr.get(counter);
				// id
				int itemId = siteItem.getId();
				// unit
				String unit = siteItem.getUnit();
				// number
				int number = siteItem.getNumber();
				System.out.println("Id: " + itemId + " unit: " + unit + " number " + number );
				
				// add to sql
				preparedStatement.setInt(1, itemId);
				preparedStatement.setInt(2, 1);
				preparedStatement.setInt(3, number);
				preparedStatement.setString(4, unit);
				// add item
				preparedStatement.addBatch();
		    } 
			// excute
			int[] updateCounts = preparedStatement.executeBatch();
            System.out.println(Arrays.toString(updateCounts));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void saveSiteItem(int siteId, int itemId, int number, String unit) {
		String sql = "UPDATE siteItem SET quantity = ? , unit = ? WHERE itemId = ? AND siteId = ?";
		ArrayList params = new ArrayList();
		System.out.println("AAAAAAAA  " + number);
		params.add(number);
		params.add(unit);
		params.add(itemId);
		params.add(siteId);
		ResultSet res = Mariadb.getInstance().query(sql, params);
	}
	public int getQuantitySiteItem(int siteId, int itemId) {
		int quantity = 0;
		String sql = "SELECT quantity FROM siteItem WHERE siteId = ? AND itemId = ?";
		ArrayList params = new ArrayList();
		params.add(siteId);
		params.add(itemId);
		ResultSet res = Mariadb.getInstance().query(sql, params);
		try {
			res.next();
			quantity = res.getInt("quantity");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return quantity;
	}
	public void updateQuantityNumber(int siteId, int itemId, int number) {
		String sql = "UPDATE siteItem SET quantity = quantity - ? WHERE itemId = ? AND siteId = ?";
		ArrayList params = new ArrayList();
		params.add(number);
		params.add(itemId);
		params.add(siteId);
		ResultSet res = Mariadb.getInstance().query(sql, params);
	}
}
