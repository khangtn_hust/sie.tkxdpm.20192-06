package Models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import Database.Mariadb;
import RequiredItem.AddItemElementGUI;

public class ListRequiredItem {

	private ArrayList itemArr = new ArrayList();
	private String name;
	private int id;
	
	public ListRequiredItem(int id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public ArrayList getItemArr(int id) {
			String sql = "SELECT requiredItem.*, items.name FROM requiredItem, items WHERE listId = ? AND items.id = requiredItem.itemId AND requiredItem.status = 1";
			ArrayList arr = new ArrayList();
			arr.add(id);
			ArrayList newItemArr = new ArrayList();
			ResultSet res = Mariadb.getInstance().query(sql, arr);
			try {
				while(res.next()) {
					// get unit
					String unit = res.getString("unit");
					// get name
					String itemName = res.getString("name");
					// get number
					int number = res.getInt("number");
					// get date
					int day = res.getInt("day");
					// get month
					int month = res.getInt("month");
					// get year
					int year = res.getInt("year");
					// get id
					int itemId = res.getInt("itemId");
					System.out.println("item: " + id + " number: " + number);
					// new LRI
					RequiredItem requiredItem = new RequiredItem(itemId, unit, number, itemName, day, month, year);
					newItemArr.add(requiredItem);
				}	
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return newItemArr;
	}
	
	public int createListItem(String name) {
		int id = 0;
		// sql insert list name
		String insertSql = "INSERT INTO listRequiredItem(name) VALUES(?)";
		ArrayList insertArr = new ArrayList();
		insertArr.add(name);
		// sql get last id
		String getIdSql = "SELECT id FROM listRequiredItem ORDER BY id DESC LIMIT 1";
		ArrayList getIdArr = new ArrayList();
		
		// excute SQL
		ResultSet insertRes = Mariadb.getInstance().query(insertSql, insertArr);
		ResultSet getIdRes = Mariadb.getInstance().query(getIdSql, getIdArr);
		try {
			getIdRes.next();
			id = getIdRes.getInt("id");
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return id;
	}
	public void saveRequiredItem(int listId, ArrayList itemArr) {
		
		String sql = "INSERT INTO requiredItem(itemId, listId, unit, number, day, month, year) VALUES (?,?,?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement = Mariadb.getInstance().getConn().prepareStatement(sql);
			for (int counter = 0; counter < itemArr.size(); counter++) { 	
				AddItemElementGUI addItemElementGUI = (AddItemElementGUI)  itemArr.get(counter);
				// id
				int id = addItemElementGUI.getId();
				// unit
				String unit = addItemElementGUI.getUnit();
				// number
				int number = addItemElementGUI.getNumber();
				// day
				int day = addItemElementGUI.getDay();
				// month
				int month = addItemElementGUI.getMonth();
				// year
				int year = addItemElementGUI.getYear();
				// name list
				System.out.println("Id: " + id + " unit: " + unit + " number " + number + " day " + day + " month " + month + " year " + year );
				
				// add to sql
				preparedStatement.setInt(1, id);
				preparedStatement.setInt(2, listId);
				preparedStatement.setString(3, unit);
				preparedStatement.setInt(4, number);
				preparedStatement.setInt(5, day);
				preparedStatement.setInt(6, month);
				preparedStatement.setInt(7, year);
				// add item
				preparedStatement.addBatch();
		    } 
			// excute
			int[] updateCounts = preparedStatement.executeBatch();
            System.out.println(Arrays.toString(updateCounts));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public void deleteList(int id) {
		String sql = "UPDATE listRequiredItem SET status = 0 WHERE id = ?";
		ArrayList arr = new ArrayList();
		arr.add(id);
		ResultSet res = Mariadb.getInstance().query(sql, arr);
	}
	
	// delete item from list
	public void deleteRequiredItem(int itemId, int listId) {
		String sql = "UPDATE requiredItem SET status = 0 WHERE itemId = ? AND listId = ?";
		ArrayList arr = new ArrayList();
		arr.add(itemId);
		arr.add(listId);
		ResultSet res = Mariadb.getInstance().query(sql, arr);
	}
	
	public void updateRequiredItem(int id, String unit, int number, int day, int month, int year, int listId) {
		String sql = "UPDATE requiredItem SET unit = ?, number = ?, day = ?, month = ?, year = ? WHERE itemId = ? AND listId = ?";
		ArrayList arr = new ArrayList();
		arr.add(unit);
		arr.add(number);
		arr.add(day);
		arr.add(month);
		arr.add(year);
		arr.add(id);
		arr.add(listId);
		ResultSet res = Mariadb.getInstance().query(sql, arr);
	}
	
	public void updateStatusList(int id, int status) {
		String sql = "UPDATE listRequiredItem SET status = ? WHERE id = ?";
		ArrayList params = new ArrayList();
		params.add(status);
		params.add(id);
		Mariadb.getInstance().query(sql, params);
	}
	
	public ArrayList getAllListOfList() {
		String sql = "SELECT * FROM listRequiredItem WHERE status = 1";
		ArrayList arr = new ArrayList();
		ArrayList listArr = new ArrayList();
		
		ResultSet res = Mariadb.getInstance().query(sql, arr);
		try {
			while(res.next()) {
				// get name
				String name = res.getString("name");
				// get id
				int id = res.getInt("id");
				System.out.println("item: " + id + " name: " + name);
				// new LRI
				ListRequiredItem LRI = new ListRequiredItem(id, name);
				listArr.add(LRI);
			}	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listArr;
	}
	
	public void setItemArr(ArrayList itemArr) {
		this.itemArr = itemArr;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
