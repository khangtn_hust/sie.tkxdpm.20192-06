package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Database.Mariadb;

public class Item {
	private int id;
	private String unit;
	private int number;
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	
	public Item(int id, String name) {
		this.id = id;
		this.name = name;
	}
	public Item(int id, String name, int quantity, String unit) {
		this.id = id;
		this.name = name;
		this.number = quantity;
		this.unit = unit;
	}
	public ArrayList getAllItem(){
		String sql = "SELECT * FROM items";
		ArrayList arr = new ArrayList();
		
		ResultSet res = Mariadb.getInstance().query(sql, arr);
		try {
			while(res.next()) {
				String itemName = res.getString("name");
				// get id
				int itemId = res.getInt("id");
				System.out.println("item: " + itemId + " name: " + itemName);
				// new LRI
				Item item = new Item (itemId, itemName);
				arr.add(item);
			}	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return arr;
	}
	public boolean addItemToDb(Item item) {
		String sql = "INSERT INTO items(id, name, quantity, unit) VALUES (?, ?, ?, ?)";
		ArrayList params = new ArrayList();
		params.add(item.getId());
		params.add(item.getName());
		params.add(item.getNumber());
		params.add(item.getUnit());
		Mariadb.getInstance().query(sql, params);
		return true;
	}
	public boolean editItemToDb(Item item) {
		String sql = "UPDATE items SET id = ?, name = ?, quantity = ?, unit = ? WHERE id = ?";
		
		ArrayList params = new ArrayList();
		params.add(item.getId());
		params.add(item.getName());
		params.add(item.getNumber());
		params.add(item.getUnit());
		params.add(item.getId());
		Mariadb.getInstance().query(sql, params);
		return true;
	}
	public boolean deleteItemFromDb(Item item) {
		String sql = "DELETE from items WHERE id = ?";
		
		ArrayList params = new ArrayList();
		params.add(item.getId());
		Mariadb.getInstance().query(sql, params);
		return true;
	}
	
}
