package Models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Database.Mariadb;

public class Transportation {
	private String name;
	private String active;
	private int numberOfDeliveryDay;
	private int id;
	
	public Transportation(String name, String active, int numberOfDeliveryDay, int id) {
		this.name = name;
		this.active = active;
		this.numberOfDeliveryDay = numberOfDeliveryDay;
		this.id = id;
	};
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getNumberOfDeliveryDay() {
		return numberOfDeliveryDay;
	}

	public void setNumberOfDeliveryDay(int numberOfDeliveryDay) {
		this.numberOfDeliveryDay = numberOfDeliveryDay;
	}

	public ArrayList getAllTransportation(int siteId) {

		ArrayList<Transportation> listTrans = new ArrayList();
		
		String sql = "SELECT TS.*, T.name as name FROM transportationSite TS, transportation T WHERE siteId = ? AND TS.transportationId = T.id";
		ArrayList params = new ArrayList();
		params.add(siteId);
		
		ResultSet res = Mariadb.getInstance().query(sql, params);
		try {
			while(res.next()) {
				// get id trans
				int transId = res.getInt("transportationId");
				// get trans name
				String transName = res.getString("name");
				// get number day
				int numberDay = res.getInt("numberOfDayDelivery");
				// get active
				String active = res.getString("active");
				
				Transportation t = new Transportation(transName, active, numberDay, transId);
				listTrans.add(t);
			}	
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listTrans;
	}
	public void saveTrans(int siteId, int transId, int number, String active) {
		String sql = "UPDATE transportationSite SET numberOfDayDelivery = ? , active = ? WHERE transportationId = ? AND siteId = ?";
		ArrayList params = new ArrayList();
		System.out.println("AAAAAAAA  " + number);
		params.add(number);
		params.add(active);
		params.add(transId);
		params.add(siteId);
		ResultSet res = Mariadb.getInstance().query(sql, params);
	}
}
