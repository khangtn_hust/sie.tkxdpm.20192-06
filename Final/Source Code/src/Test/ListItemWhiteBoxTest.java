package Test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import RequiredItem.RequiredItemValidator;

class ListItemWhiteBoxTest {

	@Test
	void checkValidDateWhiteBoxTest() {
		int dayArr[] = {29, 30, 29, 28, 30, 31, 31, 32};
		int monthArr[] = {2, 2, 2, 2, 4 ,4 , 3 ,3};
		int yearArr[] = {2016, 2016, 2017, 2017, 2016 , 2016, 2016, 2016};
		boolean inputCheckArr[] = {true, false, false, true, true, false, true, false};
		
		for(int i = 0; i< dayArr.length; i++) {
			boolean exp = inputCheckArr[i];
			boolean res = RequiredItemValidator.dateInputValidator(dayArr[i], monthArr[i], yearArr[i]);
			assertEquals(exp, res, "Khong phai ngay hop le");
		}
	}
}
