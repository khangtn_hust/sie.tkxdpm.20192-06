package Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import RequiredItem.RequiredItemValidator;

class ListItemBlackBoxTest {

	@Test
	void checkValidDateBlackBoxTest() {
		int dayArr[] = {31, 31, 29, 29};
		int monthArr[] = {1, 2, 2, 2};
		int yearArr[] = {2020, 2020, 2016, 2017};
		boolean inputCheckArr[] = {true, false, true, false};
		
		for(int i = 0; i< dayArr.length; i++) {
			boolean exp = inputCheckArr[i];
			boolean res = RequiredItemValidator.dateInputValidator(dayArr[i], monthArr[i], yearArr[i]);
			assertEquals(exp, res, "Khong phai ngay hop le");
		}
	}
	
}
