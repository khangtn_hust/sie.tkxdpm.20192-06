package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Mariadb{
	
	private static Mariadb instance;
	
	private String JDBC_DRIVER = "org.mariadb.jdbc.Driver";
	private String DB_URL = "jdbc:mariadb://128.199.239.13/pttkxdpm";

    //  Database credentials
	private String USER = "root";
	private String PASS = "gn1C4IrvFiGesuwK";
    
    // gi gi fo
    private Statement stmt = null;
    private Connection conn = null;
    
    // constructor
    private Mariadb() {
    	super();
    	try {
			connect();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static synchronized Mariadb getInstance() {
    	if(instance == null) {
    		instance = new Mariadb();
    	} 
    	return instance;
    }
    // connect Db
    public void connect() throws ClassNotFoundException, SQLException {
    	
       //STEP 2: Register JDBC driver
         Class.forName(JDBC_DRIVER);

         //STEP 3: Open a connection
         System.out.println("Connecting to a selected database...");
         conn = DriverManager.getConnection(
        		 DB_URL, USER, PASS);
         System.out.println("Connected database successfully...");
         stmt = conn.createStatement();
    }
	
    public Statement getStmt() {
		return stmt;
	}

	public void setStmt(Statement stmt) {
		this.stmt = stmt;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public ResultSet query(String sql, ArrayList list) {
    	ResultSet res = null;
		try {
			// prepare for sql query
		    PreparedStatement preStatement = conn.prepareStatement(sql);
		    Object[] arr = list.toArray();
		    // add argument
		    for (int i = 0; i < arr.length; i++) {
		    	if (arr[i] instanceof Integer) {
		    		preStatement.setInt(i+1, (int) arr[i]);
		        }
		        else if (arr[i] instanceof String) {
		        	preStatement.setString(i+1, (String) arr[i]);
		        }
	        }
		    System.out.println(preStatement.toString());
			res = preStatement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return res;
    }
}
