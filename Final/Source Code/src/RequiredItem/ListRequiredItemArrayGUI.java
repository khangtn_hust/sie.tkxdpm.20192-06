package RequiredItem;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

import Interface.IParentScreen;
import Models.ListRequiredItem;
import Orders.OrderController;

public class ListRequiredItemArrayGUI extends JPanel implements IParentScreen{
	private boolean nvbh;
	ListRequiredItemController listRequiredItemController;
	RequiredItemMenu requiredItemMenu;
	IParentScreen parentGUI;
	ListRequiredItemArrayGUI that;
	/**
	 * Create the panel.
	 */
	public ListRequiredItemArrayGUI(IParentScreen parentGUI, boolean nvbh) {
		this.nvbh = nvbh;
		this.parentGUI = parentGUI;
		that = this;
		listRequiredItemController = new ListRequiredItemController();
		init();
		
	}
	public void init() {
		this.setBounds(6, 6, 859, 546);
		removeAll();
		revalidate();
		repaint();
		setLayout(new BorderLayout(0, 0));
		
		// Content panel, chua list
		Panel contentPanel = new Panel();
		add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		
		// get list
		ArrayList listArr = listRequiredItemController.getAllListOfList();
		
		// Object luu data
		Object[][] data = new Object[listArr.size()][3];
		// tao ds
		for (int counter = 0; counter < listArr.size(); counter++) { 		      
			ListRequiredItem LRI = (ListRequiredItem) listArr.get(counter);
			// Them du lieu vao obj data
			data[counter][0] = LRI.getId(); 
			data[counter][1] = LRI.getName();
	    }
		// Ten cac cot
		String[] columnName = {"Id", "Tên danh sách"};
		// overide some mothod jTable
		JTable table = new JTable(data, columnName) {
			public boolean isCellEditable(int rowIndex, int columnIndex){
				if (columnIndex == 0 || columnIndex == 1) return false;
					return true;
				}
			};
		contentPanel.add(table);
				
		contentPanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
		// Panel button bottom
		Panel buttonPanel = new Panel();
		add(buttonPanel, BorderLayout.SOUTH);
		JButton btnBack = new JButton("Back");
		buttonPanel.add(btnBack);
		
		// header button
		Panel actionPanel = new Panel();
		add(actionPanel, BorderLayout.NORTH);
		
		Button detailBtn = new Button("Xem");
		detailBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = (int) table.getValueAt(table.getSelectedRow(),0);
				String name = (String) table.getValueAt(table.getSelectedRow(),1);
				ListRequiredItemGUI LRIG = new ListRequiredItemGUI(id, name, that);
				removeAll();
				add(LRIG);
				revalidate();
				repaint();
			}
		});
		actionPanel.add(detailBtn);
		// neu la nvbh thi dc xoa
		if(nvbh) {
			Button btnDelete = new Button("Delete");
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					int input = JOptionPane.showOptionDialog(null, "Bạn đã chắc chưa????", "Thông báo", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
					// neu an "NO"
					if(input == JOptionPane.NO_OPTION) {
						System.out.println("Click NO");
					} else if(input == JOptionPane.YES_OPTION) {
						System.out.println("Click yes");
						// Thuc hien xoa
						System.out.println(table.getValueAt(table.getSelectedRow(),1));
						int id = (int) table.getValueAt(table.getSelectedRow(),0);
						listRequiredItemController.deleteList(id);
						// Hien thong bao thanh cong\
						int check = JOptionPane.showOptionDialog(null, "Đã xoá thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(check == JOptionPane.OK_OPTION)
						{
							// refresh
							init();
						}
						
					}
				}
			});
			actionPanel.add(btnDelete);
		} else {
			// neu la nhan vien quan ly thi dc dat hang
			Button btnCreateOrder = new Button("Tạo đơn hàng");
			btnCreateOrder.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					OrderController orderController = new OrderController();
					int id = (int) table.getValueAt(table.getSelectedRow(),0);
					// Thuc hien update
					boolean check = orderController.createOrder(id);
					// Hien thong bao thanh cong\
					if(check) {
						int input = JOptionPane.showOptionDialog(null, "Tạo đơn hàng thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							init();
						}
					} else {
						int input = JOptionPane.showOptionDialog(null, "Không thành công, không đủ số lượng", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							init();
						}
					}	
				}
			});
			actionPanel.add(btnCreateOrder);
		}
		
		// back btn action
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				parentGUI.init();
			}
		});
	}

}
