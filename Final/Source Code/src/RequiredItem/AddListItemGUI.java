package RequiredItem;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Models.RequiredItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AddListItemGUI extends JPanel {
	AddListItemGUI that;
	RequiredItemMenu requiredItemMenu;
	ArrayList listItemArr;
	JTextField textFieldName ;
	ListRequiredItemController listRequiredItemController;
	/**
	 * Create the panel.
	 */
	public AddListItemGUI(RequiredItemMenu requiredItemMenu) {
		that = this;
		listRequiredItemController = new ListRequiredItemController();
		this.requiredItemMenu = requiredItemMenu;
		setLayout(new BorderLayout(0, 0));
		init();
		
	}
	void init() {
		this.setBounds(6, 6, 859, 546);
		listItemArr = new ArrayList();
		Panel btnPanel = new Panel();
		add(btnPanel, BorderLayout.SOUTH);
		
		JButton btnAddItem = new JButton("Add item");
		btnPanel.add(btnAddItem);
			
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String listName = textFieldName.getText();
				// them list vao db
				boolean check = listRequiredItemController.createListItem(listName, listItemArr);
				// Kiem tra thanh cong roi hien thong bao
				if(check) {
					int input = JOptionPane.showOptionDialog(null, "Tao thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						requiredItemMenu.init();
					}
					} else {
						int input = JOptionPane.showOptionDialog(null, "Không thành công, kiểm tra đầu vào", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							// requiredItemMenu.init();
						}
					}
					
				}
		});
		btnPanel.add(btnSave);
			
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				requiredItemMenu.init();
			}
		});
		btnPanel.add(btnBack);
			
		Panel nameListPanel = new Panel();
		add(nameListPanel, BorderLayout.NORTH);
			
		JLabel lblListName = new JLabel("Tên đơn hàng");
		nameListPanel.add(lblListName);
			
		// Ten don hang
		textFieldName = new JTextField();
		textFieldName.setColumns(12);
		nameListPanel.add(textFieldName);
			
		Panel listPanel = new Panel();
		add(listPanel, BorderLayout.CENTER);
		listPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
			
		// first item
		AddItemElementGUI addItemElementGUI = new AddItemElementGUI();
		listPanel.add(addItemElementGUI);
		listItemArr.add(addItemElementGUI);
			
		btnAddItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddItemElementGUI addItemElementGUI = new AddItemElementGUI();
				// them o giao dien
				listPanel.add(addItemElementGUI);
				// them vao mang data
				listItemArr.add(addItemElementGUI);
				that.revalidate();
				that.repaint();
			}
		});
	}
}
