package RequiredItem;

public class RequiredItemValidator {
	static int MAX_VALID_YR = 9999; 
	static int MIN_VALID_YR = 1800; 
	  
	// Returns true if  
	// given year is valid. 
	static boolean isLeap(int year) 
	{ 
	        // Return true if year is  
	        // a multiple of 4 and not  
	        // multiple of 100. 
	        // OR year is multiple of 400. 
		return (((year % 4 == 0) &&  
	           (year % 100 != 0)) ||  
	           (year % 400 == 0)); 
	 } 
	public static boolean dateInputValidator(int day, int month, int year) {
		// If year, month and day  
        // are not in given range 
        if (year > MAX_VALID_YR ||  
        		year < MIN_VALID_YR) 
            return false; 
        if (month < 1 || month > 12) 
            return false; 
        if (day < 1 || day > 31) 
            return false; 
  
        // Handle February month 
        // with leap year 
        if (month == 2)  
        { 
            if (isLeap(year)) 
                return (day <= 29); 
            else
                return (day <= 28); 
        } 
  
        // Months of April, June,  
        // Sept and Nov must have  
        // number of days less than 
        // or equal to 30. 
        if (month == 4 || month == 6 ||  
        	month == 9 || month == 11) 
            return (day <= 30); 
  
        return true; 
	}
	public static boolean checkIsInt(int number) {
		if(number == (int) number) {
			return true;
		}
		return false;
	}
}
