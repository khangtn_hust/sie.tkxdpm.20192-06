package RequiredItem;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import Interface.IParentScreen;
import Models.RequiredItem;

public class ListRequiredItemGUI extends JPanel {
	private int id;
	private JTable table;
	private String name;
	IParentScreen iParentScreen;
	ListRequiredItemGUI that;
	/**
	 * Create the panel.
	 */
	public ListRequiredItemGUI(int id, String name, IParentScreen iParentScreen) {
		this.id = id;
		this.iParentScreen = iParentScreen;
		this.name = name;
		that = this;
		init();
		
	}
	void init() {
		this.setBounds(6, 6, 859, 546);
		removeAll();
		revalidate();
		repaint();
		
		// Lay thong tin cua 1 don hang tren id
		ListRequiredItemController listRequiredItemController = new ListRequiredItemController();
		ArrayList itemArr = listRequiredItemController.getAllListItem(id);
		
		// Object luu data
		Object[][] data = new Object[itemArr.size()][7];
		// Data ban dau ko thay doi
		Object[][] oldData = new Object[itemArr.size()][7];

		// tao ds
		for (int counter = 0; counter < itemArr.size(); counter++) { 		      
			RequiredItem requiredItem = (RequiredItem) itemArr.get(counter);
			// Them du lieu vao obj data
			data[counter][0] = requiredItem.getId(); 
			data[counter][1] = requiredItem.getName();
			data[counter][2] = requiredItem.getUnit();
			data[counter][3] = requiredItem.getNumber();
			data[counter][4] = requiredItem.getDay();
			data[counter][5] = requiredItem.getMonth();
			data[counter][6] = requiredItem.getYear();
		} 
		
		// copy data
		for(int i = 0; i<itemArr.size(); i++) {
			for(int j = 0; j<7; j++) {
				oldData[i][j] = data[i][j];
			}
		}
		// Ten cac cot
		String[] columnName = {"Id", "Tên sản phẩm", "Đơn vị", "Số lượng", "Ngày", "Tháng", "Năm"};
 		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// kiem tra xem thang nao thay doi
				boolean[] change = { false, false, false, false, false, false, false };
				for(int i = 0; i <itemArr.size(); i++) {
					for(int j=0; j<7; j++) {
						if(table.getValueAt(i,j) != oldData[i][j]) {
							change[i] = true;
						}
					}
				}
				// lay ra nhung row thay doi
				for(int i = 0; i<7; i++) {
					if(change[i]) {
						try {
							// params: itemId, unit, number, day, month, year, listId
							int itemId = (int) data[i][0];
							String unit = (String) data[i][2];
							int number = Integer.parseInt((data[i][3].toString()));
							int day = Integer.parseInt((data[i][4]).toString());
							int month =Integer.parseInt((data[i][5]).toString());
							int year = Integer.parseInt((data[i][6]).toString());
							// Thuc hien update
							boolean check = listRequiredItemController.updateRequiredItem(itemId, unit, number, day, month, year, id);
							// Hien thong bao thanh cong\
							if(check) {
								int input = JOptionPane.showOptionDialog(null, "Update thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

								if(input == JOptionPane.OK_OPTION)
								{
									init();
								}
							} else {
								int input = JOptionPane.showOptionDialog(null, "Update unsuccessfully, invalid input", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

								if(input == JOptionPane.OK_OPTION)
								{
									init();
								}
							}	
						} catch(Exception err) {
							int check = JOptionPane.showOptionDialog(null, "Update unsuccessfully, invalid input", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
						}
						
					}
				}
				
			}
		});
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(table.getValueAt(table.getSelectedRow(),1));
				int itemId = (int) table.getValueAt(table.getSelectedRow(),0);
				
				// Hien thong bao
				int input = JOptionPane.showOptionDialog(null, "Bạn đã chắc chưa????", "Thông báo", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
				// neu an "NO"
				if(input == JOptionPane.NO_OPTION) {
					System.out.println("Click NO");
				} else if(input == JOptionPane.YES_OPTION) {
					System.out.println("Click yes");
					// Thuc hien xoa
					listRequiredItemController.deleteRequiredItem(itemId, id);
					// Hien thong bao thanh cong\
					int check = JOptionPane.showOptionDialog(null, "Đã xoá thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(check == JOptionPane.OK_OPTION)
					{
						// refresh
						that.init();
					}
					
				}
			}
		});
		panel.add(btnDelete);
		panel.add(btnSave);
		panel.add(btnBack);
		
		Panel listInfo = new Panel();
		add(listInfo, BorderLayout.NORTH);
		listInfo.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel listNameLabel = new JLabel(name);
		listNameLabel.setHorizontalAlignment(SwingConstants.CENTER);
		listInfo.add(listNameLabel);
		
		Panel tablePanel = new Panel();
		add(tablePanel, BorderLayout.CENTER);
		tablePanel.setLayout(new BorderLayout(0, 0));
		
		// overide some mothod jTable
		table = new JTable(data, columnName) {
			public boolean isCellEditable(int rowIndex, int columnIndex){
				if (columnIndex == 0 || columnIndex == 1) return false;
				return true;
			}
		};
		tablePanel.add(table);
		
		tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
	}

}
