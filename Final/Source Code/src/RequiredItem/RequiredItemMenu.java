package RequiredItem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import Interface.IParentScreen;

public class RequiredItemMenu extends JPanel implements IParentScreen{
	RequiredItemMenu that;
	IParentScreen iParentScreen;
	/**
	 * Create the panel.
	 */
	public RequiredItemMenu(IParentScreen iParentScreen) {
		this.iParentScreen = iParentScreen;
		that = this;
		init();
	}
	public void init() {
		setLayout(null);
		
		removeAll();
		repaint();
		JPanel thisPanel = this;
		
		JButton btnTaoDs = new JButton("Tạo DS");
		btnTaoDs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				thisPanel.removeAll();
				// Danh sach cac list don hang
				AddListItemGUI addItemGUI = new AddListItemGUI(that);
				thisPanel.add(addItemGUI);
				thisPanel.revalidate();
				thisPanel.repaint();
			}
		});
		btnTaoDs.setBounds(374, 158, 88, 29);
		add(btnTaoDs);
		
		// Move to view list
		JButton btnXemDs = new JButton("Xem DS");
		btnXemDs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				thisPanel.removeAll();
				// Danh sach cac list don hang
				ListRequiredItemArrayGUI LLRI = new ListRequiredItemArrayGUI(that, true);
				thisPanel.add(LLRI);
				thisPanel.revalidate();
				thisPanel.repaint();
			}
		});
		
		btnXemDs.setBounds(374, 293, 92, 29);
		add(btnXemDs);
		
		this.setBounds(6, 6, 859, 546);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		btnBack.setBounds(374, 511, 117, 29);
		add(btnBack);
	}
}
