package RequiredItem;

import java.util.ArrayList;

import Models.Item;
import Models.ListRequiredItem;

public class ListRequiredItemController {
	ListRequiredItem LRI;
	public ListRequiredItemController() {
		LRI = new ListRequiredItem(1, "");
	}
	// Lay tat ca danh sach yeu cau
	public ArrayList getAllListItem(int id) {
		 return LRI.getItemArr(id);
	}
	// Xoa danh sach
	public void deleteList(int id) {
		LRI.deleteList(id);
	}
	// Xoa mat hang trong danh sach
	public void deleteRequiredItem(int itemId, int listId) {
		LRI.deleteRequiredItem(itemId, listId);
	}
	// Cap nhat mat hang trong danh sacb
	public boolean updateRequiredItem(int id, String unit, int number, int day, int month, int year, int listId) {
		// Check it is a valid number
		boolean checkNumber = RequiredItemValidator.checkIsInt(number);
		// Check Valid date
		boolean checkDate = RequiredItemValidator.dateInputValidator(day, month, year);
		
		if(!checkNumber || !checkDate) {
			return false;
		}
		LRI.updateRequiredItem(id, unit, number, day, month, year, listId);
		return true;
	}
	// Lay tat ca ten cac danh sach
	public ArrayList getAllListOfList() {
		  return LRI.getAllListOfList();
	}
	// Lay tat ca mat hang co the dat
	public ArrayList getAllItem() {
		  Item item = new Item(1, null);
		  return item.getAllItem();
	 }
	 // Tao danh sach moi
	 public boolean createListItem(String listName, ArrayList listItemArr) {
		 try {
			 // Validator
			 for (int counter = 0; counter < listItemArr.size(); counter++) { 	
					AddItemElementGUI addItemElementGUI = (AddItemElementGUI)  listItemArr.get(counter);
					// number
					int number = addItemElementGUI.getNumber();
					// Check it is a valid number
					boolean checkNumber = RequiredItemValidator.checkIsInt(number);
					// day
					int day = addItemElementGUI.getDay();
					// month
					int month = addItemElementGUI.getMonth();
					// year
					int year = addItemElementGUI.getYear();
					// Check Valid date
					boolean checkDate = RequiredItemValidator.dateInputValidator(day, month, year);
					if(!checkNumber || !checkDate) {
						return false;
					}
			 } 
			 // save list required Item
			 LRI = new ListRequiredItem(1, "");
			 int listId = LRI.createListItem(listName);
			 System.out.println("id: " + listId);
			 
			 // save required item
			 LRI.saveRequiredItem(listId, listItemArr);	
			 return true;
		 } catch(Exception e) {
			 return false;
		 }
	 }
}
