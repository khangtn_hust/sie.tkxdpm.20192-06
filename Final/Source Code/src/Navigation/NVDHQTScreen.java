package Navigation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import Interface.IParentScreen;
import Orders.ListOrderGUI;
import RequiredItem.ListRequiredItemArrayGUI;

public class NVDHQTScreen extends JPanel implements IParentScreen{
	NVDHQTScreen that;
	/**
	 * Create the panel.
	 */
	public NVDHQTScreen() {
		that = this;
		init();
	}
	public void init() {
		
		this.setBounds(6, 6, 859, 546);
		setLayout(null);
		removeAll();
		repaint();
		JButton btnGetListRequiredItem = new JButton("Xem danh sách đơn cần đặt");
		btnGetListRequiredItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				that.add(new ListRequiredItemArrayGUI(that, false));
				revalidate();
				repaint();
			}
		});
		btnGetListRequiredItem.setBounds(315, 109, 237, 29);
		add(btnGetListRequiredItem);
		
		JButton btnGetListOrder = new JButton("Xem danh sách đơn hàng đã đặt");
		btnGetListOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				that.add(new ListOrderGUI(that));
				revalidate();
				repaint();
			}
		});
		btnGetListOrder.setBounds(315, 339, 237, 29);
		add(btnGetListOrder);
	}

}
