package Navigation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import Interface.IParentScreen;
import Orders.WaitingOrderGUI;
import Sites.SiteItemGUI;
import Sites.TransportationGUI;

public class SiteScreen extends JPanel implements IParentScreen{
	private SiteScreen that;
	/**
	 * Create the panel.
	 */
	public SiteScreen() {
		that = this;
		init();
	}
	public void init() {
		this.setBounds(6, 6, 859, 546);
		setLayout(null);
		// reset
		removeAll();
		revalidate();
		repaint();
		
		JButton btnItemSite = new JButton("Quản lý mặt hàng kinh doanh");
		btnItemSite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				add(new SiteItemGUI(that));
				revalidate();
				repaint();
			}
		});
		btnItemSite.setBounds(297, 108, 256, 29);
		add(btnItemSite);
		
		JButton btnTransportation = new JButton("Quản lý vận chuyển");
		btnTransportation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				add(new TransportationGUI(that));
				revalidate();
				repaint();
			}
		});
		btnTransportation.setBounds(297, 244, 256, 29);
		add(btnTransportation);
		
		JButton btnOrders = new JButton("Danh sách đơn hàng cần xác nhận");
		btnOrders.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				add(new WaitingOrderGUI(that));
				revalidate();
				repaint();
			}
		});
		btnOrders.setBounds(292, 366, 272, 29);
		add(btnOrders);
	}

}
