package Navigation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import Interface.IParentScreen;
import Item.ItemGUI;
import RequiredItem.RequiredItemMenu;

public class NVBHScreen extends JPanel implements IParentScreen{
	NVBHScreen that;
	/**
	 * Create the panel.
	 */
	public NVBHScreen() {
		init();
		that = this;
	}
	public void init() {
		this.setBounds(6, 6, 859, 546);
		setLayout(null);
		removeAll();
		repaint();
		
		JButton btnItem = new JButton("Quản lý mặt hàng");
		btnItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				repaint();
				that.add(new ItemGUI(that));
			}
		});
		btnItem.setBounds(311, 121, 202, 29);
		add(btnItem);
		
		JButton btnRequiredItem = new JButton("Quản lý mặt hàng cần đặt");
		btnRequiredItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				repaint();
				that.add(new RequiredItemMenu(that));
			}
		});
		btnRequiredItem.setBounds(311, 271, 202, 29);
		add(btnRequiredItem);
	}
}
