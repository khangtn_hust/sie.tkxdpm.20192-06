package Navigation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import Interface.IParentScreen;
import Orders.ReceiveOrderGUI;

public class NVQLKScreen extends JPanel implements IParentScreen{

	NVQLKScreen that;
	/**
	 * Create the panel.
	 */
	public NVQLKScreen() {
		that = this;
		init();
	}
	public void init() {
		
		this.setBounds(6, 6, 859, 546);
		setLayout(null);
		removeAll();
		repaint();
		JButton btnGetListRequiredItem = new JButton("Xem danh sách đơn chờ vận chuyển");
		btnGetListRequiredItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeAll();
				that.add(new ReceiveOrderGUI(that));
				revalidate();
				repaint();
			}
		});
		btnGetListRequiredItem.setBounds(270, 251, 271, 29);
		add(btnGetListRequiredItem);
	}
}
