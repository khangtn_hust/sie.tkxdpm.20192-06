import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Item.ItemGUI;
import Navigation.NVBHScreen;
import Navigation.NVDHQTScreen;
import Navigation.NVQLKScreen;
import Navigation.SiteScreen;

public class Login extends JPanel {
	private JTextField usernameInput;
	private JTextField passwordInput;

	/**
	 * Create the panel.
	 */
	public Login(application app) {
		setForeground(Color.GREEN);
		setLayout(null);
		
		usernameInput = new JTextField();
		usernameInput.setBounds(187, 84, 130, 26);
		add(usernameInput);
		usernameInput.setColumns(10);
		
		passwordInput = new JPasswordField();
		// passwordInput.setEchoChar('*');
		passwordInput.setBounds(187, 149, 130, 26);
		add(passwordInput);
		passwordInput.setColumns(10);
		
		
		JLabel usernameLabel = new JLabel("Username");
		usernameLabel.setBounds(85, 89, 90, 16);
		add(usernameLabel);
		
		JLabel passwordLabel = new JLabel("Password");
		passwordLabel.setBounds(85, 154, 61, 16);
		add(passwordLabel);
		
		JLabel wrongpassText = new JLabel("Mật khẩu sai");
		wrongpassText.setForeground(Color.RED);
		wrongpassText.setFont(new Font("Lucida Grande", Font.ITALIC, 13));
		wrongpassText.setBounds(169, 187, 80, 16);
		wrongpassText.setVisible(false);
		add(wrongpassText);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = usernameInput.getText();
				String password = passwordInput.getText();
				
				if(password.equals("123456")) {
//					RequiredItemMenu requiredItemMenu = new RequiredItemMenu();
					app.getFrame().getContentPane().removeAll();
					app.getFrame().repaint();
					if(username.equals("nvbh")) {
						NVBHScreen nvbhScreen = new NVBHScreen();
						System.out.println("Nhan vien ban hang dang nhap");
					    app.getFrame().getContentPane().add(nvbhScreen);
					}
					if(username.equals("nvdhqt")) {
						System.out.println("Nhan vien dat hang dang nhap");
						NVDHQTScreen nvdhScreen = new NVDHQTScreen();
						ItemGUI itemGUI = new ItemGUI();
					    app.getFrame().getContentPane().add(nvdhScreen);
					}
					if(username.equals("site")) {
						System.out.println("Site dang nhap");
						SiteScreen siteScreen = new SiteScreen();
					    app.getFrame().getContentPane().add(siteScreen);
					}
					if(username.equals("nvqlk")) {
						System.out.println("Nhan vien quan ly kho dang nhap");
						NVQLKScreen ncqlkScreen = new NVQLKScreen();
					    app.getFrame().getContentPane().add(ncqlkScreen);
					}
				} else {
					wrongpassText.setVisible(true);
				}
			}
		});
		btnLogin.setBounds(144, 213, 117, 29);
		add(btnLogin);
	}
}
