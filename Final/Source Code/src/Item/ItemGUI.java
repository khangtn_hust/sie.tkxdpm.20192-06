package Item;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Interface.IParentScreen;
import Navigation.NVBHScreen;
import RequiredItem.ListRequiredItemArrayGUI;

public class ItemGUI extends JPanel {
	ItemGUI that;
	NVBHScreen nvbhScreen;
	/**
	 * Create the panel.
	 */
	public ItemGUI() {
		System.out.print("parent");
		setLayout(null);
		
		JButton btnToDs = new JButton("Tạo DS");
		btnToDs.setBounds(385, 283, 88, 29);
		add(btnToDs);
		
		JButton btnXemDs = new JButton("Xem DS");
		btnXemDs.setBounds(381, 169, 92, 29);
		add(btnXemDs);
		
//		JButton btnBack = new JButton("Back");
//		btnBack.setBounds(381, 350, 92, 29);
//		add(btnBack);
//		btnBack.addActionListener(new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				// TODO Auto-generated method stub
//				iParentScreen.init();
//			}
//		});
		
		
		JLabel lblNewLabel = new JLabel("Nhan vien quan ly");
		lblNewLabel.setBounds(375, 80, 113, 16);
		add(lblNewLabel);
		this.setBounds(6, 6, 859, 546);
	}

	public ItemGUI(NVBHScreen nvbhscreen) {
		that = this;
		this.nvbhScreen = nvbhscreen;
		System.out.print("screen");
		init();
		
	}
	public void init() {
		// TODO Auto-generated constructor stub
				this.setBounds(6, 6, 859, 546);
				removeAll();
				revalidate();
				repaint();
				
				setLayout(null);
				
				JButton btnToDs = new JButton("Tạo DS");
				btnToDs.setBounds(385, 283, 88, 29);
				add(btnToDs);
				btnToDs.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
							
					}
				});
				
				JButton btnXemDs = new JButton("Xem DS");
				btnXemDs.setBounds(381, 169, 92, 29);
				add(btnXemDs);
				btnXemDs.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						removeAll();
						// Danh sach cac list don hang
						ListItemGUI listItemGUI = new ListItemGUI(that);
						add(listItemGUI);
						revalidate();
						repaint();
					}
				});
				
				JButton btnBack = new JButton("Back");
				btnBack.setBounds(381, 350, 92, 29);
				add(btnBack);
				btnBack.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						nvbhScreen.init();
					}
				});
				
				
				JLabel lblNewLabel = new JLabel("Nhan vien quan ly");
				lblNewLabel.setBounds(375, 80, 113, 16);
				add(lblNewLabel);
				this.setBounds(6, 6, 859, 546);
	}
}
