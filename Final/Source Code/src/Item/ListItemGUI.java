package Item;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import Models.Item;
import Models.RequiredItem;

public class ListItemGUI extends JPanel implements ActionListener{
	private JTable table;
	Object[][] data;
	ItemGUI itemGUI;
	public ListItemGUI(ItemGUI itemGUI) {
		this.itemGUI = itemGUI;
		init();
	}
	private void init() {
		refreshGUI();
		drawTableOfItem();
		drawToolBar();
	}
	private void drawToolBar() {
		JPanel toolBar = new JPanel();
		add(toolBar, BorderLayout.NORTH);
		JButton btnBack = new JButton("Back");
		JButton btnAdd = new JButton("Add");
		JButton btnDelete = new JButton("Delete");
		JButton btnEdit = new JButton("Edit");
		
		btnAdd.addActionListener(this);
		btnEdit.addActionListener(this);
		btnDelete.addActionListener(this);
		btnBack.addActionListener(this);

		
		toolBar.add(btnAdd);
		toolBar.add(btnEdit);
		toolBar.add(btnDelete);
		toolBar.add(btnBack);
		add(toolBar, BorderLayout.NORTH);
	}
	private void drawTableOfItem() {
		ArrayList<Item> listItem = (new ListItemController()).getAllItem();
		data = new Object[listItem.size()][4];
		for(int i=0; i<listItem.size(); i++) {
			data[i][0] = listItem.get(i).getId();
			data[i][1] = listItem.get(i).getName();
			data[i][2] = listItem.get(i).getNumber();
			data[i][3] = listItem.get(i).getUnit();

		}

		String[] columnName = {"Id", "Tên sản phẩm", "Đơn vị", "Số lượng"};
 		setLayout(new BorderLayout(0, 0));
		
 		Panel tablePanel = new Panel();
		add(tablePanel, BorderLayout.CENTER);
		tablePanel.setLayout(new BorderLayout(0, 0));
		
		table = new JTable(data, columnName) {
			public boolean isCellEditable(int rowIndex, int columnIndex){
				if (columnIndex == 0 || columnIndex == 1) return false;
				return true;
			}
		};
		tablePanel.add(table);
		tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
		
	}
	private void refreshGUI() {
		this.setBounds(6, 6, 859, 546);
		removeAll();
		revalidate();
		repaint();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		String choice = e.getActionCommand();
		if(choice.equals("Add")) {
			showAddDialog();
			
		}else if(choice.equals("Edit")){
			showEditDialog();
		
		}else if(choice.equals("Delete")) {
			showDeleteDialog();
			
		}else {
			System.out.println("Back");
			itemGUI.init();;
		}
	}
	private void showDeleteDialog() {
		int selectedRow = table.getSelectedRow();
		if(selectedRow>=0) {
			// TODO Auto-generated method stub
			Object[] option = {"Yes, do it","Hell no!!!"};
			int n = JOptionPane.showOptionDialog(
					SwingUtilities.getWindowAncestor(this), "Would u like to xóa this?", 
					"Delete", JOptionPane.YES_NO_OPTION, 
					JOptionPane.WARNING_MESSAGE, null, 
					option, option[0]);
			if(n==JOptionPane.YES_OPTION) {
				Item item = new Item(Integer.parseInt(data[selectedRow][0].toString()),
						data[selectedRow][1].toString(),
						Integer.parseInt(data[selectedRow][2].toString()),
						data[selectedRow][3].toString());
				item.deleteItemFromDb(item);
				init();
			}
		}
	
	}
	private void showEditDialog() {
		// TODO Auto-generated method stub
		System.out.println("edit  item");
		int selectedRow = table.getSelectedRow();
		System.out.println(selectedRow);
		if(selectedRow>=0) {
			
			DialogEdit dialogEdit = new DialogEdit(
					(JFrame) SwingUtilities.getWindowAncestor(this), 
					new Item(Integer.parseInt(data[selectedRow][0].toString()),
							data[selectedRow][1].toString(),
							Integer.parseInt(data[selectedRow][2].toString()),
							data[selectedRow][3].toString()));
		
			dialogEdit.setVisible(true);
			Item item = dialogEdit.run();
			if(item!=null) {
				item.editItemToDb(item);
			}
			this.init();
		}
		
	}
	private void showAddDialog() {
		System.out.println("add new item");
		DialogAdd dialogAdd = new DialogAdd((JFrame) SwingUtilities.getWindowAncestor(this), ListItem.getLastItem());
		dialogAdd.setVisible(true);
		Item item = dialogAdd.run();
		if(item!=null) {
			item.addItemToDb(item);
		}
		this.init();
		// TODO Auto-generated method stub
	}
}
