package Item;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import Models.Item;

public class DialogAdd extends JDialog implements ActionListener{
	private String[] data = null;
	public Item lastItem;
	JPanel panel;
	JTextField tfID, tfName, tfUnit, tfQuantity;
	public DialogAdd(Frame parent, Item lastItem) {
		super(parent, "Add", true);
		//set location of dialog
		this.lastItem = lastItem;
	      setLocation(parent.getLocation().x+100,parent.getLocation().y+100);
		data = new String[4];
		
		UIdesign();
		
	}
	
	protected void UIdesign() {
		// TODO Auto-generated method stub
		setBounds(350, 300, 350, 200);

		panel = new JPanel();
		panel.setBounds(0, 0, 100, 100);
		add(panel);
		panel.setLayout(new GridLayout(0, 2, 5, 10));
		JLabel lableID = new JLabel("ID", SwingConstants.CENTER);
		 tfID = new JTextField(lastItem.getId()+1 +"");
		 tfID.setEditable(false);
		JLabel lableName = new JLabel("Name", SwingConstants.CENTER);
		 tfName = new JTextField();
		JLabel lableQuantity = new JLabel("Quantity", SwingConstants.CENTER);
		 tfQuantity = new JTextField();
		JLabel lableUnit = new JLabel("Unit", SwingConstants.CENTER);
		 tfUnit = new JTextField();
		
		
		panel.add(lableID); panel.add(tfID);
		panel.add(lableName); panel.add(tfName);
		panel.add(lableQuantity); panel.add(tfQuantity);
		panel.add(lableUnit); panel.add(tfUnit);
		
		addButton();
		

	}

	private void addButton() {
		JButton btSave = new JButton("Save");
		JButton btCancel = new JButton("Cancel");
		panel.add(btSave);
		panel.add(btCancel);
		btSave.addActionListener(this);
		btCancel.addActionListener(this);		
	}

	public void actionPerformed(ActionEvent e) {
		String choice = e.getActionCommand();
		if(choice.equals("Save")) {
			saveNewItem();
		}else {
			cancelThisTask();
		}
	}
	
	private void cancelThisTask() {
		// TODO Auto-generated method stub
		System.out.println("cancel");
		data = null;
		dispose();
	}

	private void saveNewItem() {
		// TODO Auto-generated method stub
		data[0] = tfID.getText();
		data[1] = tfName.getText();
		data[2] = tfQuantity.getText();
		data[3] = tfUnit.getText();
		dispose();
	}

	public Item run() {
//		this.setVisible(true);
		if(data==null) return null;
		else return new Item(Integer.parseInt(data[0]), data[1], Integer.parseInt(data[2]), data[3]);
	}
}
