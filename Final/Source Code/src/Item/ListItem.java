package Item;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Database.Mariadb;
import Models.Item;

public class ListItem {
		public static ArrayList<Item> getListItem(){
			String sql = "SELECT * FROM items";
			ArrayList arr = new ArrayList();
			
			ResultSet res = Mariadb.getInstance().query(sql, arr);
			try {
				while(res.next()) {
					String itemName = res.getString("name");
					// get id
					int itemId = res.getInt("id");
					int itemQuantity = res.getInt("quantity");
					String itemUnit = res.getString("unit");
					System.out.println("item: " + itemId + " name: " + itemName);
					// new LRI
					Item item = new Item (itemId, itemName, itemQuantity, itemUnit);
					arr.add(item);
				}	
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return arr;
		}
		public static Item getLastItem() {
			ArrayList arr = getListItem();
			Item lastItem = (Item)arr.get(arr.size()-1);
			return lastItem;
		}
}
