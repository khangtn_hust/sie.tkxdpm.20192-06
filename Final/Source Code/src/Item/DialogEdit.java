package Item;

import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import Models.Item;

public class DialogEdit extends DialogAdd{
	public DialogEdit(Frame parent, Item lastItem) {
		super(parent, lastItem);
		System.out.println(lastItem.getName());
		// TODO Auto-generated constructor stub
	}
	protected void UIdesign() {
		// TODO Auto-generated method stub
		setBounds(350, 300, 350, 200);

		panel = new JPanel();
		panel.setBounds(0, 0, 100, 100);
		add(panel);
		panel.setLayout(new GridLayout(0, 2, 5, 10));
		JLabel lableID = new JLabel("ID", SwingConstants.CENTER);
		 tfID = new JTextField(lastItem.getId() +"");
		 tfID.setEditable(false);
		JLabel lableName = new JLabel("Name", SwingConstants.CENTER);
		 tfName = new JTextField(lastItem.getName());
		JLabel lableQuantity = new JLabel("Quantity", SwingConstants.CENTER);
		 tfQuantity = new JTextField(lastItem.getNumber()+"");
		JLabel lableUnit = new JLabel("Unit", SwingConstants.CENTER);
		 tfUnit = new JTextField(lastItem.getUnit());
		
		
		panel.add(lableID); panel.add(tfID);
		panel.add(lableName); panel.add(tfName);
		panel.add(lableQuantity); panel.add(tfQuantity);
		panel.add(lableUnit); panel.add(tfUnit);
		
		addButton();
		

	}
	private void addButton() {
		JButton btSave = new JButton("Save");
		JButton btCancel = new JButton("Cancel");
		panel.add(btSave);
		panel.add(btCancel);
		btSave.addActionListener(this);
		btCancel.addActionListener(this);		
	}
}
