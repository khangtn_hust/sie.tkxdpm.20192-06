package Sites;

import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Models.Item;

public class SiteItemElementGUI extends JPanel {

	private JTextField textUnit;
	private JComboBox cbItemName;
	private JTextField textNumber;
	
	private SiteItemController itemController;

	String[] itemNameArr;
	int[] itemIdArr;
	
	private int id;
	private String unit;
	private int number;
	private String name;
	
	public int getId() {
		int index = cbItemName.getSelectedIndex();
		id = itemIdArr[index];
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUnit() {
		unit = textUnit.getText();
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getNumber() {
		number = Integer.parseInt(textNumber.getText());
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * Create the panel.
	 */
	public SiteItemElementGUI() {
		itemController = new SiteItemController();
		this.setBounds(6, 6, 859, 97);
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		 // day la them item
		JLabel lblItem = new JLabel("Tên sản phẩm: ");
		// get from database
		ArrayList itemArr = itemController.getAvalableItem();
			
		// save into Arr
		itemNameArr = new String[itemArr.size()];
		itemIdArr = new int[itemArr.size()];
		
		// attach to combobox
		for (int counter = 0; counter < itemArr.size(); counter++) { 		      
			Item item = (Item) itemArr.get(counter);
			itemNameArr[counter] = item.getName();
			itemIdArr[counter] = item.getId();
		} 
		// comboBox id
		cbItemName = new JComboBox(itemNameArr);
		cbItemName.setMaximumRowCount(30);
		add(cbItemName);
		// Label Unit
		JLabel lblUnit = new JLabel(" Đơn vị");
		add(lblUnit);
		// Textfield Unit
		
		textUnit = new JTextField();
		add(textUnit);
		textUnit.setColumns(10);
		// Lable Number
		JLabel lblNumber = new JLabel("Số lượng");
		add(lblNumber);
		// TExtfield Number
		textNumber = new JTextField();
		add(textNumber);
		textNumber.setColumns(5);	
	}
}
