package Sites;

import java.awt.BorderLayout;
import java.awt.Panel;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;

import Interface.IParentScreen;
import Models.Transportation;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class TransportationGUI extends JPanel {
	private TransportationController transController;
	private JTable table;
	private IParentScreen iParentScreen;
	/**
	 * Create the panel.
	 */
	public TransportationGUI(IParentScreen iParentScreen) {
		transController = new TransportationController();
		this.iParentScreen = iParentScreen;
		init();
	}
	
	void init() {
		this.setBounds(6, 6, 859, 546);
		removeAll();
		revalidate();
		repaint();
		setLayout(new BorderLayout(0, 0));
		
		// panel contain table
		Panel tablePanel = new Panel();
		add(tablePanel);
		tablePanel.setLayout(new BorderLayout(0, 0));
		
		// panel at bottom
		JPanel panelBtn = new JPanel();
		add(panelBtn, BorderLayout.SOUTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		panelBtn.add(btnBack);
		
		ArrayList transArr = transController.getAllTrans();
		
		// Object luu data
		Object[][] data = new Object[transArr.size()][4];

		// tao ds
		for (int counter = 0; counter < transArr.size(); counter++) { 		      
			Transportation trans = (Transportation) transArr.get(counter);
			// Thong tin van chuyen
			data[counter][0] = trans.getId();
			data[counter][1] = trans.getName();
			data[counter][2] = trans.getNumberOfDeliveryDay();
			data[counter][3] = trans.getActive();
		} 
		
		String[] columnName = {"Id","Tên", "Số ngày", "Có hỗ trợ"};
		// overide some mothod jTable
		table = new JTable(data, columnName) {
			public boolean isCellEditable(int rowIndex, int columnIndex){
				if (columnIndex == 0 || columnIndex == 1) return false;
					return true;
				}
		};
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean check = transController.updateTrans(data, transArr.size(), 4);
				if(check) {
					int input = JOptionPane.showOptionDialog(null, "Update thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						init();
					}
				} else {
					int input = JOptionPane.showOptionDialog(null, "Update không thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						init();
					}
				}
			}
		});
		panelBtn.add(btnSave);
		tablePanel.add(table);
				
		tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
	}
}
