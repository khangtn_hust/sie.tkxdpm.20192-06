package Sites;

import java.util.ArrayList;

import Models.Transportation;

public class TransportationController {
	
	private Transportation trans;
	
	public TransportationController() {
		trans = new Transportation(null, null, 0, 0);
	}
	
	public ArrayList getAllTrans() {
		ArrayList transArr = trans.getAllTransportation(1);
		return transArr;
	}
	
	public boolean updateTrans(Object[][] data, int width, int height) {
		boolean res;
		try {
			int siteId = 1;
			for(int i = 0; i < width; i++) {
				int transId = Integer.parseInt(data[i][0].toString()); 
				int number = Integer.parseInt(data[i][2].toString()); 
				String active = (String) data[i][3];
				trans.saveTrans(siteId, transId, number, active);
			}
			res = true;
		}catch(Exception e) {
			System.out.print(e);
			res = false;
		}
		return res;
	}
}
