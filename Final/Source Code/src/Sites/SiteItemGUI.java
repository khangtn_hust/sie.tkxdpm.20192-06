package Sites;

import java.awt.BorderLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Interface.IParentScreen;
import Models.SiteItem;
import java.awt.FlowLayout;
import java.awt.Label;
import javax.swing.BoxLayout;
import java.awt.CardLayout;

public class SiteItemGUI extends JPanel {

	private SiteItemController itemController;
	private JTable table;
	private IParentScreen iParentScreen;
	private ArrayList<SiteItemElementGUI> listItemAdded;
	/**
	 * Create the panel.
	 */
	public SiteItemGUI(IParentScreen iParentScreen) {
		listItemAdded = new ArrayList();
		itemController = new SiteItemController();
		this.iParentScreen = iParentScreen;
		init();
	}
	
	void init() {
		this.setBounds(6, 6, 859, 546);
		removeAll();
		revalidate();
		repaint();
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		
		// panel contain table
		Panel tablePanel = new Panel();
		add(tablePanel);
		tablePanel.setLayout(new BorderLayout(0, 0));
		
		
		String[] columnName = {"Id Sản phẩm", "Tên sản phẩm", "Số lượng", "Đơn vị"};
			
		Panel addPanel = new Panel();
		add(addPanel);
		addPanel.setSize(getMaximumSize());
		addPanel.setLayout(new CardLayout(0, 0));
		
		Panel addItemPanel = new Panel();
		addPanel.add(addItemPanel, "name_204814187720662");
		
		// panel at bottom
		JPanel panelBtn = new JPanel();
		add(panelBtn);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		panelBtn.add(btnBack);
		
		ArrayList itemArr = itemController.getAllItems();
		
		// Object luu data
		Object[][] data = new Object[itemArr.size()][4];
		// Data ban dau ko thay doi
		Object[][] oldData = new Object[itemArr.size()][4];

		// tao ds
		for (int counter = 0; counter < itemArr.size(); counter++) { 		      
			SiteItem item = (SiteItem) itemArr.get(counter);
			// Thong tin van chuyen
			data[counter][0] = item.getItemId();
			data[counter][1] = item.getItemName();
			data[counter][2] = item.getQuantity();
			data[counter][3] = item.getUnit();
		} 
		
		// copy data
		for(int i = 0; i<itemArr.size(); i++) {
			for(int j = 0; j<4; j++) {
				oldData[i][j] = data[i][j];
			}
		}
		
		// overide some mothod jTable
		table = new JTable(data, columnName) {
			public boolean isCellEditable(int rowIndex, int columnIndex){
				if (columnIndex == 0 || columnIndex == 1) return false;
					return true;
				}
		}; 
				
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SiteItemElementGUI siteItemElement = new SiteItemElementGUI();
				listItemAdded.add(siteItemElement);
				addItemPanel.add(siteItemElement);
				// removeAll();
				revalidate();
				repaint();
			}
		});
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Them nhung item moi
				boolean addCheck = itemController.addSiteItem(listItemAdded);
				// Luu lai nhung thay doi
				boolean saveCheck = itemController.updateSiteItem(data, itemArr.size(), 4);
				if(addCheck && saveCheck) {
					int input = JOptionPane.showOptionDialog(null, "Cap nhat thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						init();
					}
				} else {
					int input = JOptionPane.showOptionDialog(null, "Cap nhat thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						init();
					}
				}
				// CAp nhat nhung item cu
				init();
			}
		});
		panelBtn.add(btnSave);
		tablePanel.add(table, BorderLayout.CENTER);
		
		tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
		panelBtn.add(btnAdd);
		
	}
}
