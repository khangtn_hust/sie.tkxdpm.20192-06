package Sites;

import java.util.ArrayList;

import Models.Item;
import Models.SiteItem;

public class SiteItemController {
private SiteItem siteItem;
	
	public SiteItemController() {
		siteItem = new SiteItem(0, null, 0, null);
	}
	
	public ArrayList getAllItems() {
		ArrayList<SiteItem> itemArr = siteItem.getAllItem(1);
		return itemArr;
	}
	
	public ArrayList getAvalableItem() {
		  Item item = new Item(1, null);
		  return item.getAllItem();
	 }
	
	public boolean addSiteItem(ArrayList itemArr) {
		try {
			siteItem.addSiteItem(itemArr);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	public boolean updateSiteItem(Object[][] data, int width, int height) {
		boolean res;
		try {
			int siteId = 1;
			for(int i = 0; i < width; i++) {
				int itemId = Integer.parseInt(data[i][0].toString()); 
				int number = Integer.parseInt(data[i][2].toString()); 
				String unit = (String) data[i][3];
				siteItem.saveSiteItem(siteId, itemId, number, unit);
			}
			res = true;
		}catch(Exception e) {
			System.out.print(e);
			res = false;
		}
		return res;
	}
}
