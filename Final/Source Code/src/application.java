import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JTextField;

import Database.Mariadb;
import RequiredItem.RequiredItemMenu;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.JLabel;

public class application {

	private JFrame frame;

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					application window = new application();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public application() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 871, 580);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		Login login = new Login(this);
		login.setBounds(240, 113, 377, 300);
		
		frame.getContentPane().add(login);
		login.setLayout(null);
		
		JList list = new JList();
		list.setBounds(67, 492, 49, -62);
		frame.getContentPane().add(list);
		
		JLabel lblNewLabel = new JLabel("Nhân viên bán hàng: nvbh");
		lblNewLabel.setBounds(50, 442, 219, 16);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNhnVint = new JLabel("Nhân viên đặt hàng quốc tế: nvdhqt");
		lblNhnVint.setBounds(50, 470, 247, 16);
		frame.getContentPane().add(lblNhnVint);
		
		JLabel lblSiteSite = new JLabel("Site: site");
		lblSiteSite.setBounds(50, 407, 219, 16);
		frame.getContentPane().add(lblSiteSite);
		
		JLabel lblNVQLK = new JLabel("Nhân viên quản lý kho: nvqlk");
		lblNVQLK.setBounds(50, 508, 219, 16);
		frame.getContentPane().add(lblNVQLK);
		
		JLabel lblMtKhu = new JLabel("Mật khẩu: 123456");
		lblMtKhu.setBounds(50, 536, 219, 16);
		frame.getContentPane().add(lblMtKhu);
	}
}
