package Orders;

import java.util.ArrayList;

import Models.ListRequiredItem;
import Models.Order;
import Models.RequiredItem;
import Models.SiteItem;

public class OrderController {
	ListRequiredItem listRequiredItem;
	SiteItem siteItem;
	Order order;
	
	public OrderController() {
		listRequiredItem = new ListRequiredItem(0, null);
		siteItem = new SiteItem(0, null, 0, null);
		order = new Order(0, 0, 0, 0, 0, null, null, null, null);
	}
	
	public boolean createOrder(int listRequiredId) {
		boolean res = true;
		// lay danh sach item trong list can dat
		try {
			ArrayList itemList = listRequiredItem.getItemArr(listRequiredId);
			for(int i = 0; i< itemList.size(); i++) {
				System.out.println("So mat hang can dat la: ");
				RequiredItem item = (RequiredItem) itemList.get(i);
				boolean check = createOrderByItem(item.getId(), item.getNumber());
				if(!check) return false;
			}
			listRequiredItem.updateStatusList(listRequiredId, 2);
		} catch(Exception e) {
			res = false;
		}
		return res;
	}
	
	private boolean createOrderByItem(int itemId, int number) {
		boolean enough = true;
		System.out.println("Mat hang: " + itemId + " can so luong: " + number);
		ArrayList<Order> siteItem = order.getAvailableSite(itemId, number);
		// neu arr rong
		if(siteItem.size() == 0) enough = false;
		for(int i = 0; i< siteItem.size(); i++) {
			Order a = siteItem.get(i);
			System.out.println("ITem: " + a.getItemId() + " so luong: " + a.getQuantity() + " tu site: " + a.getSiteId());
			order.createOrder(a.getItemId(), a.getSiteId(), a.getQuantity());
		}
		return enough;
	}
	
	public ArrayList getAllOrder() {
		ArrayList<Order> list= order.getAllOrder();
		return list;
	}
	public ArrayList getWaitingOrder() {
		ArrayList<Order> list = order.getOrderBySite(1);
		return list;
	}
	
	public boolean confirmOrder(int orderId, int itemId, int numberRequired) {
		// tam fix cung
		int siteId = 1;
		boolean check = true;
		try {
			// kiem tra so luong hang trong kho
			int remainQuantity = siteItem.getQuantitySiteItem(siteId, itemId);
			// neu du hang
			if(remainQuantity >= numberRequired) {
				// tru di so luong trong kho
				siteItem.updateQuantityNumber(siteId, itemId, numberRequired);
				// cap nhat trang thai order
				order.updateStatusOrder(orderId, 1);
			} else {
				return false;
			}
			
		} catch(Exception e) {
			check = false;
		}
		return check;
	}
	
	public boolean deleteOrder(int orderId) {
		boolean check = true;
		try {
			order.updateStatusOrder(orderId, 3);
		} catch(Exception e) {
			check = false;
		}
		return check;
	}
	public ArrayList getConfirmOrder(){
		ArrayList<Order> listOrder = order.getOrderByStatus(1);
		return listOrder;
	}
	public boolean updateConfirmOrder(int orderId, String note) {
		boolean check = true;
		try {
			order.updateConfirmOrder(orderId, 2, note);
		}catch (Exception e) {
			check = false;
		}
		return check;
	}
}
