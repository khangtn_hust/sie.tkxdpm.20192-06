package Orders;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import Interface.IParentScreen;
import Models.Order;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ReceiveOrderGUI extends JPanel implements IParentScreen{
	private OrderController orderController;
	private IParentScreen iParentScreen;
	/**
	 * Create the panel.
	 */
	public ReceiveOrderGUI(IParentScreen iParentScreen) {
		orderController = new OrderController();
		this.iParentScreen = iParentScreen;
		init();
	}
	public void init(){
		removeAll();
		revalidate();
		repaint();

		setLayout(new BorderLayout(0, 0));
		
		Panel dataPanel = new Panel();
		
		dataPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		Panel buttonPanel = new Panel();
		add(buttonPanel, BorderLayout.SOUTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		buttonPanel.add(btnBack);
	
		// add data
		ArrayList<Order> listOrder = orderController.getConfirmOrder();
		for(int i = 0; i< listOrder.size(); i++) {
			Order order = listOrder.get(i);
			int orderId = order.getOrderId();
			String itemName = order.getItemName();
			int itemId = order.getItemId();
			int quantity = order.getQuantity();
			int status = order.getStatus();
			String siteName = order.getSiteName();
			System.out.println(itemName);
			ReceiveOrderElementGUI elementGUI = new ReceiveOrderElementGUI(orderId, itemName, quantity, status, siteName, this);
			dataPanel.add(elementGUI);
		}
		
		this.setBounds(6, 6, 859, 546);
		add(dataPanel, BorderLayout.CENTER);
	}
}
