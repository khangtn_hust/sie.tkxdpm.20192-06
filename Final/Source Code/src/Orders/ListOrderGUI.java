package Orders;

import java.awt.BorderLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTable;

import Interface.IParentScreen;
import Models.Order;
import Models.Transportation;

public class ListOrderGUI extends JPanel {
	private OrderController orderController;
	private JTable table;
	private IParentScreen iParentScreen;
	/**
	 * Create the panel.
	 */
	public ListOrderGUI(IParentScreen iParentScreen) {
		orderController = new OrderController();
		this.iParentScreen = iParentScreen;
		init();
	}
	
	void init() {
		this.setBounds(6, 6, 859, 546);
		removeAll();
		revalidate();
		repaint();
		setLayout(new BorderLayout(0, 0));
		
		// panel contain table
		Panel tablePanel = new Panel();
		add(tablePanel);
		tablePanel.setLayout(new BorderLayout(0, 0));
		
		// panel at bottom
		JPanel panelBtn = new JPanel();
		add(panelBtn, BorderLayout.SOUTH);
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		panelBtn.add(btnBack);
		
		ArrayList orderArr = orderController.getAllOrder();
		
		// Object luu data
		Object[][] data = new Object[orderArr.size()][7];

		// tao ds
		for (int counter = 0; counter < orderArr.size(); counter++) { 		      
			Order orders = (Order) orderArr.get(counter);
			data[counter][0] = orders.getOrderId();
			data[counter][1] = orders.getItemName();
			data[counter][2] = orders.getSiteName();
			data[counter][3] = orders.getQuantity();
			int status = orders.getStatus();
			if(status == 0) {
				data[counter][4] = "Đang chờ duyệt";  
			} else if(status == 1){ 
				data[counter][4] = "Đã chấp nhận";
			} else if(status == 2){ 
				data[counter][4] = "Đã nhận được hàng";  
			} else {
				data[counter][4] = "Đã xoá";  
			}
			data[counter][5] = orders.getCreatedAt();
			data[counter][6] = orders.getNote();
		} 
		
		String[] columnName = {"Id", "Tên sản phẩm", "Site cung cấp", "Số lượng", "Trạng thái", "Thời gian tạo", "Note"};
		// overide some mothod jTable
		table = new JTable(data, columnName) {
			public boolean isCellEditable(int rowIndex, int columnIndex){
				return false;
			}
		};
		tablePanel.add(table);
				
		tablePanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
	}

}
