package Orders;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.util.ArrayList;

import javax.swing.JPanel;

import Interface.IParentScreen;
import Models.Order;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class WaitingOrderGUI extends JPanel implements IParentScreen{
	private IParentScreen iParentScreen;
	private OrderController orderController;
	/**
	 * Create the panel.
	 */
	public WaitingOrderGUI(IParentScreen iParentScreen) {
		this.iParentScreen = iParentScreen;
		orderController = new OrderController();
		init();
	}
	
	public void init(){
		removeAll();
		revalidate();
		repaint();

		setLayout(new BorderLayout(0, 0));
		
		Panel dataPanel = new Panel();
		
		dataPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		Panel buttonPanel = new Panel();
		add(buttonPanel, BorderLayout.SOUTH);
		
		Button button = new Button("Back");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iParentScreen.init();
			}
		});
		buttonPanel.add(button);
		// add data
		ArrayList<Order> listOrder = orderController.getWaitingOrder();
		for(int i = 0; i< listOrder.size(); i++) {
			Order order = listOrder.get(i);
			int orderId = order.getOrderId();
			String itemName = order.getItemName();
			int itemId = order.getItemId();
			int quantity = order.getQuantity();
			int status = order.getStatus();
			WaitingOrderElementGUI elementGUI = new WaitingOrderElementGUI(orderId, itemName, quantity, status, itemId, this);
			dataPanel.add(elementGUI);
		}
		
		this.setBounds(6, 6, 859, 546);
		add(dataPanel, BorderLayout.CENTER);
	}
}
