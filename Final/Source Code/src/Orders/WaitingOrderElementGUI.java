package Orders;

import javax.swing.JPanel;

import Interface.IParentScreen;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WaitingOrderElementGUI extends JPanel {
	private int orderId;
	private String itemName;
	private int itemId;
	private int quantity;
	private int status;
	private OrderController orderController;
	
	IParentScreen iParentScreen;
	/**
	 * Create the panel.
	 */
	public WaitingOrderElementGUI(int orderId,  String itemName, int quantity, int status, int itemId, IParentScreen iParentScreen) {
		this.orderId = orderId;
		this.itemId = itemId;
		this.itemName = itemName;
		this.status = status;
		this.quantity = quantity;
		this.iParentScreen = iParentScreen;
		orderController = new OrderController();
		init();
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void init() {
		this.setBounds(6, 6, 758, 97);
		removeAll();
		revalidate();
		repaint();
		
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblOrderId = new JLabel("Mã đơn hàng:       ");
		add(lblOrderId);
		
		JLabel orderIdText = new JLabel("" + orderId);
		add(orderIdText);
		
		JLabel lblItemName = new JLabel("     Tên sản phẩm:       ");
		add(lblItemName);
		
		JLabel nameText = new JLabel(itemName);
		add(nameText);
		
		JLabel lblNumber = new JLabel("    Số lượng:    ");
		add(lblNumber);
		
		JLabel numberText = new JLabel("" + quantity);
		add(numberText);
		
		// neu don hang chua xac nhan
		if(status == 0) {
			JButton btnNewButton = new JButton("Confirm");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					boolean check = orderController.confirmOrder(orderId, itemId, quantity);
					if(check) {
						int input = JOptionPane.showOptionDialog(null, "Update thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							iParentScreen.init();
							System.out.print("An ok");
						}
					} else {
						int input = JOptionPane.showOptionDialog(null, "Khong du so luong!!!!", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							iParentScreen.init();
						}
					}
				}
			});
			add(btnNewButton);
			
			JButton btnDelete = new JButton("Delete");
			btnDelete.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					boolean check = orderController.deleteOrder(orderId);
					if(check) {
						int input = JOptionPane.showOptionDialog(null, "Update thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							iParentScreen.init();
						}
					} else {
						int input = JOptionPane.showOptionDialog(null, "Khong du so luong!!!!", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

						if(input == JOptionPane.OK_OPTION)
						{
							iParentScreen.init();
						}
					}
				}
			});
			add(btnDelete);
		} else if(status == 1){
			// neu don hang da xac nhan
			JLabel lblConfirm = new JLabel("  Đã xác nhận ");
			add(lblConfirm);
		}else {
			// neu don hang da tu choi
			JLabel lblDelete = new JLabel("  Đã từ chối ");
			add(lblDelete);
		}
		
	}
}
