package Orders;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import Interface.IParentScreen;
import javax.swing.JTextField;

public class ReceiveOrderElementGUI extends JPanel {

	private int orderId;
	private String itemName;
	private int itemId;
	private int quantity;
	private int status;
	private String siteName;
	private OrderController orderController;
	
	IParentScreen iParentScreen;
	private JTextField noteTextField;
	/**
	 * Create the panel.
	 */
	public ReceiveOrderElementGUI(int orderId,  String itemName, int quantity, int status,  String siteName, IParentScreen iParentScreen) {
		this.orderId = orderId;
		this.itemId = itemId;
		this.itemName = itemName;
		this.status = status;
		this.quantity = quantity;
		this.iParentScreen = iParentScreen;
		this.siteName = siteName;
		orderController = new OrderController();
		init();
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public void init() {
		this.setBounds(6, 6, 792, 97);
		removeAll();
		revalidate();
		repaint();
		
		setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblOrderId = new JLabel("Mã đơn hàng: ");
		add(lblOrderId);
		
		JLabel orderIdText = new JLabel("" + orderId);
		add(orderIdText);
		
		JLabel lblItemName = new JLabel("Sản phẩm: ");
		add(lblItemName);
		
		JLabel nameText = new JLabel(itemName);
		add(nameText);
		
		JLabel lblSiteName = new JLabel("  Tên site:     ");
		add(lblSiteName);
		
		JLabel siteNameText = new JLabel(siteName);
		add(siteNameText);
		JLabel lblNumber = new JLabel("Số lượng: ");
		add(lblNumber);
		
		JLabel numberText = new JLabel("" + quantity);
		add(numberText);
		
		JLabel lblNote = new JLabel(" Note");
		add(lblNote);
		
		JButton btnNewButton = new JButton("Confirm");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// nhan hang thanh cong
				String note = noteTextField.getText();
				boolean check = orderController.updateConfirmOrder(orderId, note);
				if(check) {
					int input = JOptionPane.showOptionDialog(null, "Update thành công", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						iParentScreen.init();
					}
				} else {
					int input = JOptionPane.showOptionDialog(null, "Khong thanh cong!!!!", "Thông báo", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);

					if(input == JOptionPane.OK_OPTION)
					{
						iParentScreen.init();
					}
				}
				}
			});
		
		noteTextField = new JTextField();
		add(noteTextField);
		noteTextField.setColumns(10);
		add(btnNewButton);
	}
}
