# Phân công công việc
## Công việc chung
Xác định Actor, xác định các UC, xác định đặc tả phụ trợ.
## Công việc riêng
### Nguyễn Thọ Điệp
Đặc tả UC Xác nhận đơn hàng, xem đơn hàng\
Định nghĩa từ điển thuật ngữ: OOPD – Oversea Order Place Department, Overseas Import Site, Merchandise code 
### Trịnh Ngọc Khang
Phân giã và đặc tả UC Quản lý danh sách mặt hàng cần đặt\
Định nghĩa từ điển thuật ngữ:Delivery means, Number of day delivery by ship, Number of day delivery by air
### Phùng Văn Hoàng
Đặc tả UC Quản lý danh sách mặt hàng, xem danh sách mặt hàng cần đặt\
Định nghĩa từ điển thuật ngữ: Delivery date desired, Delivery by ship, Delivery by air 
### Vũ Đức Huy
Đặc tả UC Xử lý danh sách mặt hàng, quản lý mặt hàng kinh doanh\
Định nghĩa từ điển thuật ngữ: Site code, Import site name, Other information
### Nguyễn Xuân Phú
Đặc tả UC Cập nhật thông tin vận chuyển, Cập nhật số lượng trong kho\
Định nghĩa từ điển thuật ngữ:Quantity ordered, In-stock quantity, Unit
### Dương Minh Huy
Đặc tả UC Thống kê đơn hàng, Lưu thông tin đơn háng\
Định nghĩa từ điển thuật ngữ:Import, Merchandise, Sale department
